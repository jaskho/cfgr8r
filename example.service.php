<?php

/**
 * @file
 * Drupal service access credentials
 * Specify private connection/access configuration for a particular
 *   service (eg, a database or a an external REST service).
 *
 *  Server configuration may be
 *    + Per service (global) -- all deployed instances connect
 *       to the same endpoint.
 *    + Per instance-type (eg, "production" or "test") -- all
 *       deployed instances of a particular type connect to
 *       a particular endpoint, but different types connect to
 *       different endpoints.
 *    + Per instance -- each deployed instance has, or may have
 *       its own endpoint (for example, in the case of local
 *       databases used in development).
 *  Per-instance-type and per-instance configurations
 *  may coexist in a project.  For example, multiple staging
 *  instances may point to a single database (per-instance-type)
 *  while each developer's instance points to a local database
 *  (per-instance).
 *
 * DATABASE CONFIGURATION
 * Configure databases per the normal Drupal Database API settings format.
 * See documentation in default.settings.php, and the example below.
 *
 * OTHER SERVICE CONFIGURATION
 * Consult the appropriate service integration module's documentation.
 * The cfgr8r system will only work with service integration modules that
 * make use of Drupal's Settings API, wherein configuration settings are
 * provided on bootstrap via the global $conf variable. 
 * See "REST service example" below for a general idea of how this
 * might be applied.
 * 
 * IMPORTANT NOTE:
 * Do not include credentials in project repository.
 * The point of cfgr8r's service.settings api is to keep sensitive
 * information OUT of a project's version control repository (while
 * allowing non-sensitive configuration to be version-controlled
 * and used in related build/deploy systems.)
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++
 * ++++++++++++++++++  USAGE  +++++++++++++++++++++
 * ++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Create a {service}.settings.php file for each service.  For example,
 *  + drupal-db.settings.php
 *  + email.settings.php
 *  + twitter.settings.php
 *
 * For each service settings file, add a mapping key to the $cfgr8r_svc_keys
 * array at the end of the (cfgr8r-ized) settings.php file (additional
 * details are provided in that file).
 *
 * The following strings, which are reserved for Drupal and CFGR8R, must not
 * be used as service keys:
 *  + settings
 *  + type
 *  + instance
 */

// ++++++++++++++++++++++++++++++++++++++++++
// +++++++++  Drupal db example  ++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// $databases = array (
//   'default' =>
//   array (
//     'default' =>
//     array (
//       'database' => 'example_mysql_db',
//       'username' => 'example_mysql_user',
//       'password' => 'example_mysql_password',
//       'host' => 'localhost',
//       'port' => '',
//       'driver' => 'mysql',
//       'prefix' => '',
//     ),
//   ),
// );

// ++++++++++++++++++++++++++++++++++++++++++
// ++++++++  REST service example  ++++++++++
// ++++++++++++++++++++++++++++++++++++++++++
// $conf['example-service'] = array(
//   "endpoint" => 'https://example-service.com,
//   "username" => "my_username",
//   "password" => "my_password",
// );
