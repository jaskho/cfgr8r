<?php

/**
 * @file
 * Drupal instance-specific configuration file, part of the 
 * "cfgr8r" Drupal configuration framework.
 * Detailed information can be found in the cfgr8r README file.
 *
 * Contains configuration settings specific to a particular host,
 * which may be a project-managed host (such as in a testing deployment),
 * or an "ad hoc" host (eg., a deployment on a local development machine.)
 *
 * For documentation, specify the host type ("dev", eg) and host identifier here:
 * @HostType: %host-type%
 * @Host: %host%
 * 
 */

/*
 
Example Type Settings code

<?php
$base_url = 'http://localhost:8888';

*/