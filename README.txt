 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

                  CONFIGURIFICATONIZATION
                       (aka cfgr8r)

      NOTE: This Drupal project uses a modified version of
            Drupal's settings.php file!

 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


#######################################
   ABOUT
#######################################

Cfgr8r defines and implements a scheme for managing configuration settings
for a Drupal site/project.

********************************
Implementation
********************************

Modifies Drupal's normal settings.php file as follows:
    + REMOVE: database configuration code
    + ADD:    logic to include additional settings files as detailed below


********************************
Key objectives
********************************

 + Support complex deployment topologies for team-based, version-controlled
   projects comprising multiple and arbitrary
      + team members/roles
      + deployed instance *types* (eg., "dev", "test", "stage", "production").
        Can also be used to support branching development scenarios where a
        "feature branch" cvs strategy is not in use (eg., "dev-alt-x",
        "test-alt-x", etc.)
      + deployed instances (eg., a development instance used by a particular
        developer or development team)
      + service dependencies (eg., drupal database(s), external databases,
        external services)
 + Separation of sensitive and non-sensitive configuration data so that:
    + Non-sensitive data can be VCS managed and integrated with automated processes.
    + Sensitive data can be sensibly and reliably organized (and, potentially,
      integrated with automated processes).





###############################################
   SETTINGS ARCHITECTURE
###############################################


********************************
  Base Settings
********************************

See "settings.php"

VERSION CONTROLLED: Yes

Default settings and/or settings that for any reason should be applied in
all deployed instances.

Base settings should NOT include
 + Settings that typically will vary by instance type.  For example, error
   reporting rules.
 + Sensitive data (eg., database or other service connection parameters).

Base settings may, if appropriate, be overridden by "instance type" or "instance"
settings (see below).


********************************
  (Instance) Type Settings
********************************

See "type.settings.php"

VERSION CONTROLLED: Yes

Settings, version-controlled, that are specific to a particular "instance type"
(for example "dev", "test", "stage", "production").

All deployed instances of a particular type should apply these settings as
defaults, overriding, if appropriate, any base settings.

Instance Type settings override Base settings.

Base settings may, if appropriate, be overridden by "instance" settings (see
below).


********************************
  Instance Settings
********************************

See "instance.settings.php"

VERSION CONTROLLED: Varies

Settings specific to a particular deployed instance (eg., a developer's local
machine).

Instance settings support three use-cases
 + Project-managed instances.  For example:
    + A development instance shared a team that wants to be able
      to override the project-wide "dev" instance type's settings (to compensate
      for machine idiosyncrasies, perhaps).
    + Development/testing alternative configuration scenarios.
 + Host-specific production deployments.  For example, a site that is mirrored on multiple,
   heterogeneous platforms.
 + Non-project-managed instances, eg. an instance used by a particular (set of)
   developer(s).

Instance settings override Base and Instance Type settings.


********************************
  "Service" settings Settings
********************************

See {service}.settings.php

Version controlled: NO

Sensitive data (eg., access credentials) used to connect to a particular
service, including, notably, the core Drupal database(s).







###############################################
   DRUPAL BOOTSTRAP WORKFLOW
###############################################

1. Load base settings, which controls the subsequent steps.

   REQUIRED

   File:
   settings.php



2. Load Instance Type settings

   OPTIONAL

   File:
   type.settings.php



3. Load Instance settings

   OPTIONAL

   File:
   instance.settings.php



4. Load Service settings

   REQUIRED (At a minimum, Drupal database connection must be provided)

   File(s):
    + {service A}.settings.php  # Drupal database
    + [{service B}].settings.php
     ...










###############################################
   DEVOPS/FILE MANAGEMENT (suggested)
###############################################

  Notes:
  + In directory trees, the .../drupal/... subdirectories exist in
    order to support projects which include Drupal as part of a larger
    solution.
  + The tag "{team}" in paths should be read "team or teammember"

  ------------------------------
   .gitignore
  ------------------------------
    Not ignored:
     + .deploy/*
     + sites/*/settings.php
    Ignored:
     + sites/*/*.settings.php
     + .dev


  ------------------------------
   Repo file structure
  ------------------------------
    sites/default/settings.php
    .deploy/
        host_types/
          {host type}[]/
             drupal/
                type.settings.php
        hosts/
          {host}[]/
             drupal/
                instance.settings.php



  ------------------------------
   Dev work tree file structure
  ------------------------------
    {all "repo file structure" items}
    .dev/   # Non-version controlled "sandbox"
       [{team}[]/]   # for shared instances
          deploy/
             drupal/
                instance.settings.php
                {service key}.settings.php[]



  ------------------------------
   external project file structure
  ------------------------------
    {project root}/
       resources/
          deploy/
             services/
                global/
                   drupal/
                      {service key}.settings.php[]
                host_types/
                   {host type}[]/
                      drupal/
                         {service key}.settings.php[]
                hosts/   # project-level hosts
                   {host}[]/
                      drupal/
                         {service key}.settings.php[]
             ad_hoc/    # utility storage/backup for dev teams/teammembers
                {team}[]
                   [{host}][]/]       # in case of multiple instances
                      instance.settings.php
                      {service key}.settings.php[]


    /deploy/services/{service}


  ------------------------------
   Deploy process
  ------------------------------

   NOTES
    + In dev environments, symlinking (vs copying) settings files allows for
      a simpler commit process
    + In path definitions in this section, the tag %EXT% refers to an external project
      directory


   + settings.php

     Normal git checkout


   + type.settings.php

     Symlink or copy file:
       source: /.deploy/host_types/{host type}/drupal/type.settings.php
       dest:   /sites/default/type.settings.php


   + instance.settings.php

      + Project-managed instance

           Symlink or copy file:
             source: /.deploy/hosts/{host}/drupal/instance.settings.php
             dest:   /sites/default/instance.settings.php


      + Host-specific production deployment

           Symlink or copy file:
             source: /.deploy/hosts/{host}/drupal/instance.settings.php
             dest:   /sites/default/instance.settings.php


      + Ad hoc instance
           Symlink or copy file:
             source: /.dev/[devteam/]/drupal/instance.settings.php
             dest:   /sites/default/instance.settings.php


   + {service key}.settings.php[]

      NOTE: In path definitions in this section, the tag %EXT% refers to an external project
            directory

      + Global services
           Symlink or copy file(s):
              source:  global/drupal/{service key}.settings.php[]
              dest:    /sites/default/{service key}.settings.php
           "source" path is relative to %EXT%/resources/deploy/services


      + Type-specific services
           Symlink or copy file(s):
              source:  host_types/{host_type}/drupal/{service key}.settings.php[]
              dest:    /sites/default/{service key}.settings.php
           "source" path is relative to %EXT%/resources/deploy/services


      + Instance-specific services (project-level)
           Symlink or copy file(s):
              source:  hosts/{host}/drupal/{service key}.settings.php[]
              dest:    /sites/default/{service key}.settings.php
           "source" path is relative to %EXT%/resources/deploy/services


      + Instance-specific services (ad hoc)
           Symlink or copy file(s):
              source:  {team}/[{host}/]{service key}.settings.php[]
              dest:    /sites/default/{service key}.settings.php
           "source" path is relative to %EXT%/resources/deploy/ad_hoc



  ------------------------------
   Code commit process
  ------------------------------

   NOTES
    + In path definitions in this section, the tag %EXT% refers to an external project
      directory


   + settings.php

      Normal commit

   + type.settings.php

      If not symlinking, before committing, copy file:
         source: /sites/default/type.settings.php
         dest:   /.deploy/host_types/{host type}/drupal/type.settings.php


   + instance.settings.php

      + Project-managed instance

         Pre-commit:
           If not symlinking, before committing, copy file:
            source: /sites/default/instance.settings.php
            dest:   /.deploy/hosts/{host}/drupal/instance.settings.php

      + Host-specific production deployment

         Pre-commit:
           If not symlinking, before committing, copy file:
            source: /sites/default/instance.settings.php
            dest:   /.deploy/hosts/{host}/drupal/instance.settings.php

      + Ad hoc instance

         Pre-commit:
           If not symlinking, copy file:
            source: /sites/default/instance.settings.php
            dest:   /.dev/[devteam/]/drupal/instance.settings.php
         Commit:
           If using project utility storage, copy file:
            source: /sites/default/instance.settings.php
            dest:   /.dev/[devteam/]/drupal/instance.settings.php


   + {service key}.settings.php[]

      Commit: 
        Copy file(s):
           source:  /sites/default/{service key}.settings.php[]
        According to service scope:

         + Global services
             dest:   global/drupal/{service key}.settings.php
           "dest" path is relative to %EXT%/resources/deploy/services


         + Type-specific services
             dest:   host_types/{host_type}/drupal/{service key}.settings.php
           "dest" path is relative to %EXT%/resources/deploy/services


         + Instance-specific services (project-level)
             dest:   hosts/{host}/drupal/{service key}.settings.php
           "dest" path is relative to %EXT%/resources/deploy/services


         + Instance-specific services (ad hoc)
             dest:   {team}/[{host}/]{service key}.settings.php
           "dest" path is relative to %EXT%/resources/deploy/ad_hoc


