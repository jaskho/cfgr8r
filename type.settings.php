<?php

/**
 * @file
 * Drupal host-type-specific configuration file, part of the 
 * "cfgr8r" Drupal configuration framework.
 * Detailed information can be found in the cfgr8r README file.
 *
 * Contains configuration settings specific to a particular host *type*,
 * for example "dev" or "stage".
 *
 * Type Settings override Base settings (found in settings.php) and may
 * in turn override Instance settings (found in instance.settings.php).
 *
 * This file should not be used for senstive data such as server credentials.
 * Instead, use {service}.settings.php (see the cfgr8rr README).
 *
 * This file should typically be version-controlled, OUTSIDE of the Drupal
 * sites directory tree.  The cfgr8r framework recommends the location
 *   .deploy/
 *       host_types/
 *         {host type}/
 *            drupal/
 *               type.settings.php,
 * For example: .deploy/host_types/dev/drupal/type.settings.php
 * 
 * For documentation, specify the host type ("dev", eg) here:
 * @HostType: %host-type%
 *
 */



/*
 
Example Type Settings code

<?php

// bump up memory limit
ini_set('memory_limit' , '128M' );

// Show errors
error_reporting(E_ALL | E_STRICT); 
ini_set('display_errors', true);

// Set module debug flag
$conf['my_module_debug'] = true;

*/
